#pragma once

#include "ofMain.h"

// Display List
// Handy class to keep a gl display list to quickly improve performance
class GLDisplayList{
public:
	GLuint list;
    bool allocated;
    GLDisplayList(){
        allocated = false;
    }
    ~GLDisplayList(){
        deallocate();
    }
    void draw(){
        glCallList(list);
    }
    void begin(){
        allocate();
        glNewList(list, GL_COMPILE);
    }
    void end(){
        glEndList();
    }
    void clear(){
        deallocate();
    }
protected:
    void allocate(){
        if(!allocated){
            list = glGenLists(1);
            allocated = true;
        }
    }
    void deallocate(){
        if(allocated){
            glDeleteLists(list, 1);
            allocated = false;
        }
    }
};