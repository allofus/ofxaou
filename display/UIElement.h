//
//  UIElement.h
//  LAI
//
//  Created by Chris Mullany on 10/10/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma once

#include "SimpleTimer.h"
#include "SimpleTimerEvent.h"
#include "UIElementEvent.h"
#include "InteractiveObject.h"

class UIElement : public InteractiveObject {
public:
    enum State {
        STATE_OPENING,
        STATE_OPEN,
        STATE_CLOSING,
        STATE_CLOSED
    };
    enum UIElementAlign {
        ALIGN_H_LEFT,
        ALIGN_H_RIGHT,
        ALIGN_H_CENTRE,
        ALIGN_V_LEFT,
        ALIGN_V_RIGHT,
        ALIGN_V_CENTRE,
    };
    
    UIElement();
	virtual void setup();
	virtual void update();
	virtual void draw();
    
    virtual void show();
    virtual void hide();
    virtual void stayOpen();
    
    string getStateString();
    bool getIsShown();
    bool getIsHidden();
    
    ofPoint position;
    bool centreRegPoint;
    bool doStayOpen;
    bool isEnabled;
    float timeOpening, timeOpen, timeClosing;
    float showProgress;
    int id;
    
    // events
    ofEvent<UIElementEvent> uiElementEvent;
	
protected:
    
    void onSimpleTimerEvent(SimpleTimerEvent &e);
    virtual void onStateComplete(){};
    
    SimpleTimer timer;
    State state;
    
};