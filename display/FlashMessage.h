//
//  FlashMessage.h
//
//  Created by chris@allofus.com
//  Copyright (c) 2012 AllofUs. All rights reserved.
//

#pragma once

#include "ofMain.h"
#include "UIElement.h"

class FlashMessage : public UIElement {
public:
    
    ofTrueTypeFont* font;
    ofFloatColor colour;
    ofFloatColor bgColour;
    ofImage* bgImage;
    
    float alpha;
    float messagPaddingH, messagPaddingV;
    float bgHeight;
    
    string message;
    
    FlashMessage();
    void show(string message);
    void show(string message, float y, bool doStayOpen=false);
	void draw();
    
protected:
    
    void onStateComplete();
    
};