//
//  FlashMessage.cpp
//
//  Created by Chris Mullany on 19/09/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "FlashMessage.h"

FlashMessage::FlashMessage() {
    UIElement::UIElement();
    // defaults
    font = NULL;
    bgImage = NULL;
    colour = ofFloatColor(1,1,1,1);
    bgColour = ofFloatColor(0.03,0.03,0.1,0.4);
    alpha = 0;
    message = "";
    doStayOpen = false;
    messagPaddingH = 100;
    messagPaddingV = 60;
}

void FlashMessage::show(string message) {
    //ofLogVerbose("FlashMessage::show: " + message);
    UIElement::show();
    this->message = message;
}


void FlashMessage::show(string message, float y, bool doStayOpen) {
    this->doStayOpen = doStayOpen;
    this->position.y = y;
    this->message = message;
    UIElement::show();
}

void FlashMessage::draw() {
    UIElement::draw();
    if (showProgress > 0) {
        
        alpha = showProgress;
        
        ofPushMatrix();
        glTranslatef(position.x, position.y, 0);
        
        // bg
        ofEnableAlphaBlending();
        if (bgImage == NULL) {
            ofSetColor(255*bgColour.r, 255*bgColour.g, 255*bgColour.b, 255*alpha*bgColour.a);
            ofRect(0, 0, ofGetWidth(), bgHeight);
        } else {
            ofSetColor(255, 255, 255, 255*alpha);
            bgImage->draw(0, 0, ofGetWidth(), bgImage->getHeight());
        }
        
        // message
        ofSetColor(colour.r*255, colour.g*255, colour.b*255, colour.a*255*alpha);
        if (font == NULL) {
            ofDrawBitmapString(message, position);
        } else {
            ofRectangle bounds = font->getStringBoundingBox(message, 0, 0);
            int x = messagPaddingH;
            int y = (int)font->getLineHeight() + messagPaddingV;
            font->drawString(message, x, y);
        }
        
        ofPopMatrix();
    }
}

void FlashMessage::onStateComplete() {
    //ofLogNotice("FlashMessage::onStateComplete :" + getStateString());
}

//////////////////////////////////////////////////////////////////////////////////
// custom event handlers
//////////////////////////////////////////////////////////////////////////////////

