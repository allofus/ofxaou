//
//  UIElement.cpp
//  LAI
//
//  Created by Chris Mullany on 10/10/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "UIElement.h"

UIElement::UIElement() {
    InteractiveObject::InteractiveObject();
    position = ofPoint(0, 0);
    centreRegPoint = true;
    timeOpening = timeClosing = 1.;
    timeOpen = 4.;
    state = STATE_CLOSED;
    doStayOpen = true;
    showProgress = 0;
    isEnabled = true;
    disableAllEvents();
    // events
    ofAddListener(timer.timerCompleteEvent, this, &UIElement::onSimpleTimerEvent);
}

void UIElement::setup() {
}

void UIElement::update() {
    switch (state) {
        case STATE_OPENING:
            showProgress += 1/(ofGetFrameRate()*timeOpening);
            break;
        case STATE_OPEN:
            showProgress = 1;
            break;
        case STATE_CLOSING:
            showProgress -= 1/(ofGetFrameRate()*timeOpening);
            break;
        case STATE_CLOSED:
            showProgress = 0;
            break;
        default:
            break;
    }
    showProgress = ofClamp(showProgress, 0, 1);
}

void UIElement::draw() {
}


void UIElement::show() {
    isEnabled = true;
    state = STATE_OPENING;
    timer.start(timeOpening);
}

void UIElement::stayOpen() {
    state = STATE_OPEN;
    if (!doStayOpen) {
        timer.start(timeOpen);
    }
}

void UIElement::hide() {
    isEnabled = false;
    state = STATE_CLOSING;
    timer.start(timeClosing);
}


string UIElement::getStateString() {
    switch (state) {
        case STATE_OPENING:
            return "OPENING";
            break;
        case STATE_OPEN:
            return "OPEN";
            break;
        case STATE_CLOSING:
            return "CLOSING";
            break;
        case STATE_CLOSED:
            return "CLOSED";
            break;
        default:
            return "NOT SET";
            break;
    }
}


bool UIElement::getIsShown() {
    if (state == STATE_OPEN || state == STATE_OPENING) return true;
    else return false;
}

bool UIElement::getIsHidden() {
    if (state == STATE_CLOSED) return true;
    else return false;
}

//////////////////////////////////////////////////////////////////////////////////
// custom event listeners
//////////////////////////////////////////////////////////////////////////////////

void UIElement::onSimpleTimerEvent(SimpleTimerEvent &e) {
    //ofLogNotice("UIElement::onSimpleTimerEvent "+ e.toString());
    onStateComplete();
    switch (state) {
        case STATE_OPENING:
            stayOpen();
            break;
        case STATE_OPEN:
            hide();
            break;
        case STATE_CLOSING:
            state = STATE_CLOSED;
            showProgress = 0;
            break;
        case STATE_CLOSED:
            break;
        default:
            break;
    }
}