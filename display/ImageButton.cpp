//
//  ImageButton.cpp
//  artefact
//
//  Created by Mullany, Chris (LDN-AOU) on 23/11/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "ImageButton.h"

ImageButton::ImageButton(){
    UIElement::UIElement();
    alpha = 0;
    doStayOpen = true;
    isPressed = false;
    label = "";
    labelFont = NULL;
    labelColour.set(255);
    labelColourPressed.set(255);
}

void ImageButton::setup(string imgPath, string imgOverPath) {
    enableMouseEvents();
    this->imgPath = imgPath;
    this->imgOverPath = imgOverPath;
    
    if (imgPath != "") {
        image.loadImage(imgPath);
        setSize(image.width, image.height);
    } else {
        setSize(20, 20);
    }
    if (imgOverPath != "") imageOver.loadImage(imgOverPath);
    
    if (labelFont!=NULL){
        textBlock.init(*labelFont);
        textBlock.setText(label);
        textBlock.setColor(255,255,255,255);
        textBlock.wrapTextX(image.width);
    }
}

void ImageButton::update() {
    UIElement::update();
}

void ImageButton::draw() {
    
    UIElement::draw();
    if (showProgress > 0) {
        alpha = showProgress;
        
        // draw image
        //ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, 255*alpha);
        
        if (isPressed || !isEnabled) {
            //ofSetColor(200, 0, 0, 255*alpha);
            //ofRect(x, y, width, height);
            ofSetColor(255, 255, 255, 255*alpha);
            if (imgOverPath!="") imageOver.draw(x, y);
            else if (imgPath!="") image.draw(x, y);
        } else {
            //ofSetColor(150, 0, 0, 255*alpha);
            //ofRect(x, y, width, height);
            ofSetColor(255, 255, 255, 255*alpha);
            if (imgPath!="") image.draw(x, y);
        }
        
        drawLabel();
        
    }
}

void ImageButton::drawLabel() {
    if (label==""||labelFont==NULL) return;
    
    float vCorrection =  textBlock.defaultFont.getLineHeight() - (float)textBlock.defaultFont.getSize();
    
    if (isPressed || !isEnabled) {
        ofSetColor(labelColourPressed.r, labelColourPressed.g, labelColourPressed.b, labelColourPressed.a*alpha);
    } else {
        ofSetColor(labelColour.r, labelColour.g, labelColour.b, labelColour.a*alpha);
    }
    //labelFont->drawString(label, x, y);
    int labelX = x+getWidth()*.5;
    int labelY = y+(getHeight()*.5)-10;
    textBlock.drawCenter(labelX, labelY);
}

void ImageButton::exit() {
}

//////////////////////////////////////////////////////////////////////////////////
// oF event handlers
//////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// private
//////////////////////////////////////////////////////////////////////////////////

