//
//  UIElementEvent.h
//  artefact
//
//  Created by Chris Mullany on 26/11/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma once
#include "ofMain.h"

class UIElementEvent : public ofEventArgs {
    
public:
    enum Type {
        TYPE_PRESSED,
    };
    
    Type type;
    int id;
    
    UIElementEvent(Type type, int id=-1) {
        this->type = type;
        this->id = id;
    }
    
    string toString(){
        string s = "UI Element Event, ";
        if (type == TYPE_PRESSED) s+="TYPE_PRESSED";
        else s+="TYPE_NOT_SET";
        s+=", id:" + ofToString(id);
        return s;
    }
    
    // 
    // for app wide events, use the static events property below e.g.
    //
    //      ofAddListener(UIElementEvent::events, this, &AppRenderer::onUserEvent);
    //
    // otherwise, create a new event and args scoped to your class
    //
    static ofEvent <UIElementEvent> events;
};
