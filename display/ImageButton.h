//
//  ImageButton.h
//  artefact
//
//  Created by Mullany, Chris (LDN-AOU) on 23/11/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma once
#include "UIElement.h"
#include "UIElementEvent.h"
#include "ofxTextSuite.h"
#include "ofxFTGLFont.h"

class ImageButton : public UIElement {
public:
    
    ImageButton();
	void setup(string imgPath="", string imgOverPath="");
	void update();
	void draw();
    void drawLabel();
	void exit();
    
    virtual void onRollOver(int x, int y) {
	}
	
	virtual void onRollOut() {
	}
	
	virtual void onMouseMove(int x, int y){
	}
	
	virtual void onDragOver(int x, int y, int button) {
	}
	
	virtual void onDragOutside(int x, int y, int button) {
        isPressed = false;
	}
	
	virtual void onPress(int x, int y, int button) {
        isPressed = true;
        if (!isEnabled) return;
        UIElementEvent eventArgs(UIElementEvent::TYPE_PRESSED, id);
        ofNotifyEvent(uiElementEvent, eventArgs);
	}
	
	virtual void onRelease(int x, int y, int button) {
        isPressed = false;
	}
	
	virtual void onReleaseOutside(int x, int y, int button) {
        isPressed = false;
	}
    
    bool isPressed;
    string label;
    ofxFTGLFont* labelFont;
    ofxTextBlock textBlock;
    ofColor labelColour,labelColourPressed;
    
private:
    float alpha;
    string imgPath, imgOverPath;
    ofImage image, imageOver;
};