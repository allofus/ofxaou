
#include "InteractiveObject.h"
#include "ofMain.h"
#include "ofEvents.h"

InteractiveObject::InteractiveObject() {
	disableAllEvents();
}

InteractiveObject::~InteractiveObject() {
	disableAllEvents();
}

void InteractiveObject::enableAllEvents() {
	enableMouseEvents();
	enableKeyEvents();
	enableAppEvents();
}

void InteractiveObject::disableAllEvents() {
	disableMouseEvents();
	disableKeyEvents();
	disableAppEvents();
}

void InteractiveObject::enableMouseEvents() {
	ofAddListener(ofEvents().mousePressed, this, &InteractiveObject::_mousePressed);
	ofAddListener(ofEvents().mouseMoved, this, &InteractiveObject::_mouseMoved);
	ofAddListener(ofEvents().mouseDragged, this, &InteractiveObject::_mouseDragged);
	ofAddListener(ofEvents().mouseReleased, this, &InteractiveObject::_mouseReleased);
}

void InteractiveObject::disableMouseEvents() {
	ofRemoveListener(ofEvents().mousePressed, this, &InteractiveObject::_mousePressed);
	ofRemoveListener(ofEvents().mouseMoved, this, &InteractiveObject::_mouseMoved);
	ofRemoveListener(ofEvents().mouseDragged, this, &InteractiveObject::_mouseDragged);
	ofRemoveListener(ofEvents().mouseReleased, this, &InteractiveObject::_mouseReleased);
}

void InteractiveObject::enableKeyEvents() {
	ofAddListener(ofEvents().keyPressed, this, &InteractiveObject::_keyPressed);
	ofAddListener(ofEvents().keyReleased, this, &InteractiveObject::_keyReleased);
}

void InteractiveObject::disableKeyEvents() {
	ofRemoveListener(ofEvents().keyPressed, this, &InteractiveObject::_keyPressed);
	ofRemoveListener(ofEvents().keyReleased, this, &InteractiveObject::_keyReleased);
}

void InteractiveObject::enableAppEvents() {
	ofAddListener(ofEvents().setup, this, &InteractiveObject::_setup);
	ofAddListener(ofEvents().update, this, &InteractiveObject::_update);
	ofAddListener(ofEvents().draw, this, &InteractiveObject::_draw);
	ofAddListener(ofEvents().exit, this, &InteractiveObject::_exit);
    //ofAddListener(ofEvents().windowResized, this, &InteractiveObject::_windowResized);
}

void InteractiveObject::disableAppEvents() {
	ofRemoveListener(ofEvents().setup, this, &InteractiveObject::_setup);
	ofRemoveListener(ofEvents().update, this, &InteractiveObject::_update);
	ofRemoveListener(ofEvents().draw, this, &InteractiveObject::_draw);
	ofRemoveListener(ofEvents().exit, this, &InteractiveObject::_exit);
    //ofRemoveListener(ofEvents().windowResized, this, &InteractiveObject::_windowResized);
}



void InteractiveObject::keyReleased(int key){
    //ofLogWarning(toString() + " keyReleased not implemented!");
}
void InteractiveObject::keyPressed(int key){
    //ofLogWarning(toString() + " InteractiveObject not implemented!");
}
void InteractiveObject::mouseMoved(int x, int y, int button){
    //ofLogWarning(toString() + " mouseMoved not implemented!");
}
void InteractiveObject::mouseDragged(int x, int y, int button){
    //ofLogWarning(toString() + " mouseDragged not implemented!");
}
void InteractiveObject::mousePressed(int x, int y, int button){
    //ofLogWarning(toString() + " mousePressed not implemented!");
}
void InteractiveObject::mouseReleased(int x, int y, int button){
    //ofLogWarning(toString() + " mouseReleased not implemented!");
}
void InteractiveObject::windowResized(int w, int h){
    //ofLogWarning(toString() + " windowResized not implemented!");
}



void InteractiveObject::_setup(ofEventArgs &e) {
	setup();
}

void InteractiveObject::_update(ofEventArgs &e) {
	update();
}

void InteractiveObject::_draw(ofEventArgs &e) {
	draw();
}

void InteractiveObject::_exit(ofEventArgs &e) {
	exit();
}
void InteractiveObject::_windowResized(ofEventArgs &e){
    // TODO: implemented _windowResized
}

void InteractiveObject::_mouseMoved(ofMouseEventArgs &e) {
	int x = e.x;
	int y = e.y;
    int button = e.button;
	_mouseX = x;
	_mouseY = y;
    mouseMoved(x,y,button);
}

void InteractiveObject::_mousePressed(ofMouseEventArgs &e) {
	int x = e.x;
	int y = e.y;
	int button = e.button;
	mousePressed(x,y,button);
}

void InteractiveObject::_mouseDragged(ofMouseEventArgs &e) {
	int x = e.x;
	int y = e.y;
	int button = e.button;
    mouseDragged(x,y,button);
}

void InteractiveObject::_mouseReleased(ofMouseEventArgs &e) {
	int x = e.x;
	int y = e.y;
	int button = e.button;
	mouseReleased(x,y,button);
}


void InteractiveObject::_keyPressed(ofKeyEventArgs &e) {
	int key = e.key;
	keyPressed(key);
}


void InteractiveObject::_keyReleased(ofKeyEventArgs &e) {
	int key = e.key;
	keyReleased(key);
}
