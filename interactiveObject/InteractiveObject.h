#pragma once

#include "ofMain.h"
#include "ofEvents.h"
#include "BaseObject.h"

class InteractiveObject : public BaseObject {
public:
    
	InteractiveObject();
    ~InteractiveObject();
    
    void enableAllEvents();				// enable all event callbacks
	void disableAllEvents();			// disable all event callbacks
	void enableMouseEvents();			// call this if object should receive mouse events
	void disableMouseEvents();			// call this if object doesn't need to receive mouse events (default)
	void enableKeyEvents();				// call this if object should receive key events
	void disableKeyEvents();			// call this if object doesn't need to receive key events (default)
	void enableAppEvents();				// call this if object should update/draw automatically	(default)
	void disableAppEvents();			// call this if object doesn't need to update/draw automatically
    
    virtual void keyReleased(int key);
    virtual void keyPressed(int key);
	virtual void mouseMoved(int x, int y, int button);
	virtual void mouseDragged(int x, int y, int button);
	virtual void mousePressed(int x, int y, int button);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);
    
    // extend InteractiveObject and override all or any of the following methods
	virtual void setup()	{}
	virtual void update()	{}
    virtual void draw()		{}
	virtual void exit()		{}
    
    bool isSetup;
    bool isEnabled;
    bool isDrawEnabled;
    
    
protected:
    
    float _mouseX, _mouseY;
	
    void _setup(ofEventArgs &e);
	void _update(ofEventArgs &e);
    void _draw(ofEventArgs &e);
	void _exit(ofEventArgs &e);
    void _windowResized(ofEventArgs &e);
    
	void _keyPressed(ofKeyEventArgs &e);
	void _keyReleased(ofKeyEventArgs &e);
    
    void _mouseMoved(ofMouseEventArgs &e);
    void _mouseDragged(ofMouseEventArgs &e);
	void _mousePressed(ofMouseEventArgs &e);
	void _mouseReleased(ofMouseEventArgs &e);
    
};