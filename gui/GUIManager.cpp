#include "GUIManager.h"

GUIManager::GUIManager(){
    currentPageNum = 0;
    isVisible = false;
    enableKeyEvents();
    radioPreset = NULL;
    pageList = NULL;
    presetLoadLock = false;
}

void GUIManager::setup() {
    name = "GUIManager";
    setupGUI();
    int n = pages.size();
    for (int i=0; i<n; i++) {
        ofxUILabel* title = (ofxUILabel*)pages[i]->getWidget("page");
        string label = ofToString(i+1) +  "/" + ofToString(n);
        title->setLabel(label);
    }
    isCursorVisible = false;
    ofHideCursor();
    hide();
}


ofxUICanvas* GUIManager::createPage(string title) {
    // setup gui page with common stuff
    gui = new ofxUIScrollableCanvas(111,0,UI_W,ofGetHeight()*2);
    gui->setWidgetSpacing(UI_WIDGET_SPACER);
    ((ofxUIScrollableCanvas*)gui)->setScrollAreaToScreenHeight();
    gui->setDrawBack(true);
    gui->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    gui->addWidgetDown(new ofxUILabel("page", "99/99", OFX_UI_FONT_SMALL));
    gui->addWidgetRight(new ofxUILabel("title", ofToUpper(title), OFX_UI_FONT_LARGE));
    gui->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_SMALL));
    gui->addWidgetDown(new ofxUIButton(UI_SAVE_LABEL, false, UI_BUTTON_H, UI_BUTTON_H));
    gui->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    gui->addSpacer(1, UI_BUTTON_H);
    gui->addButton("<", false);
    gui->addButton(">", false);
    gui->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    gui->addSpacer(UI_SLIDER_W, 2);
    return gui;
}

void GUIManager::setupGUI() {
    
    // Add a nice list of pages to help toggle them!
    pageList = new ofxUIScrollableCanvas(0,0,110,ofGetHeight()*2);
    
    // add gui config page
    title = "GUI";
    path = "GUI/gui.xml";
    gui = createPage(title);
    gui->addWidgetDown(new ofxUILabel("background", OFX_UI_FONT_MEDIUM));
    gui->addSlider("bg r", 0, 255, 0., UI_SLIDER_SMALL_W, UI_SLIDER_H)->setIncrement(1);
    gui->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    gui->addSlider("bg g", 0, 255, 0., UI_SLIDER_SMALL_W, UI_SLIDER_H)->setIncrement(1);
    gui->addSlider("bg b", 0, 255, 0., UI_SLIDER_SMALL_W, UI_SLIDER_H)->setIncrement(1);
    gui->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    gui->addSlider("bg a", 0, 255, 0., UI_SLIDER_W, UI_SLIDER_H)->setIncrement(1);
    setupPresets();

    
    // add listener for gui events
    ofAddListener(gui->newGUIEvent, this, &GUIManager::guiEvent);
    // add gui page to gui manager
    addPage(gui);
    // load in our existing saved settings
    gui->loadSettings(path);
    
    // finish nice list of pages
    pageList->setWidgetSpacing(UI_WIDGET_SPACER);
    ((ofxUIScrollableCanvas*)pageList)->setScrollAreaToScreenHeight();
    pageList->setDrawBack(true);
    vector<string> names;
    for (int i=0; i<pages.size(); i++) {
        ofxUILabel* title = (ofxUILabel*)pages[i]->getWidget("title");
        string label = title->getLabel();
        if (label.size()>7) label = label.substr(0,7);
        names.push_back( ofToString(i) + ":" + label );
    }
    pageList->addRadio("Pages", names, OFX_UI_ORIENTATION_VERTICAL);
    pageList->addSpacer(0, 40);
    pageList->autoSizeToFitWidgets();
    ofAddListener(pageList->newGUIEvent, this, &GUIManager::guiPageListEvent);
    
}

void GUIManager::setupPresets()
{
    gui->addSpacer();

    gui->addLabel("'ctrl + n' to add a new preset with name",OFX_UI_FONT_SMALL);
    gui->addLabel("'ctrl + s; to save/overwrite selected preset", OFX_UI_FONT_SMALL);
    gui->addLabel("'ctrl + d; to delete selected preset", OFX_UI_FONT_SMALL);
    
    gui->addSpacer();
    
    gui->addLabel("new preset name:", OFX_UI_FONT_MEDIUM);
    presetTextInput = gui->addTextInput("_PRESET", "add preset");
    presetTextInput->setAutoClear( false );
    
    gui->addSpacer();
    
    vector<string> names;
    radioPreset = gui->addRadio("radio_presets", names);

    
    
    ofDirectory presetDir = getPresetDir();
    presetDir.listDir();
    vector<string> items;
    for (int i=0; i<presetDir.numFiles(); i++)
    {
        string p = presetDir.getPath( i );
        ofDirectory dir(p);
        if(dir.exists() && dir.isDirectory())
        {
            
            int found = p.find_last_of( "/");
            string label = p.substr(found+1);
            ofLog() << "add entry for: " << label;
            ofxUIToggle* t = gui->addLabelToggle(label, false);
            radioPreset->addToggle( t );
        }
    }
    
    gui->autoSizeToFitWidgets();

}

ofDirectory GUIManager::getPresetDir()
{
    ofDirectory presetDir( "GUI/presets" );
    if(!presetDir.exists()) presetDir.create();
    return presetDir;
}

string GUIManager::getCurrentSelectedPresetName()
{
    string s = "";
    vector<ofxUIToggle*> toggles = radioPreset->getToggles();
    for (int i=0; i<toggles.size(); i++) {
        if (toggles[i]->getValue() == 1)
        {
            s = toggles[i]->getName();
        }
    }
    return s;
}

void GUIManager::addPreset()
{
    ofLog() << "ADD PRESET: ";
    ofDirectory dir = getPresetDir();
    string dirName = dir.path();
    string presetPath = dirName + "/" + newPresetName;
    ofDirectory presetDir( presetPath );
    if(!presetDir.exists())
    {
        presetDir.create();
    }
    else
    {
        ofLog() << "preset dir already exists... quit.";
        return;
    }
    
    deselectAllPresetToggles();
    
    //reset textfield for new preset name
    presetTextInput->setTextString("add preset");
    
    writePresets( presetPath );
    

    
    ofxUIToggle* t = gui->addLabelToggle(newPresetName, false);
    radioPreset->addToggle( t );
    gui->autoSizeToFitWidgets();
}

void GUIManager::savePreset()
{
    string current = getCurrentSelectedPresetName();
    ofLog()<< "save preset: " << current;
    if(current == "")
    {
        ofLog() << "no preset selected, will not save";
        return;
    }

    ofDirectory presets = getPresetDir();
    string presetPath = presets.path() + "/" + current;
    writePresets( presetPath );
}

void GUIManager::writePresets(string path)
{
    ofLog() << "-------writing presets to: " << path;
    ofDirectory dir( path );
    if(dir.exists() && dir.isDirectory())
    {
        int n = pages.size();
        for (int i=0; i<n; i++) {
            ofxUILabel* title = (ofxUILabel*)pages[i]->getWidget("title");
            string label = title->getLabel();
            
            if(presetExcludes[label])
            {
                //ofLog() << "---------------do not write preset for file: " << label;
                continue;
            }
            
            pages[i]->saveSettings(path + "/" + label + ".xml");
            ofLog() << "saving preset for: " << label;
        }
    }
}

//this comes on gui click button
void GUIManager::loadPreset()
{
    string current = getCurrentSelectedPresetName();
    if(current == "")
    {
        ofLog() << "nothing to load";
        return;
    }
    _loadPreset( current );
}

//this is normally called from outside the gui
void GUIManager::loadPreset(string value)
{
    //make sure we have a preset w/ that name
    vector<ofxUIToggle*> toggles = radioPreset->getToggles();
    string toggleName, toLoad;
    for (int i=0; i<toggles.size(); i++)
    {
        toggleName = toggles[i]->getName();
        if( value == toggleName)
        {
            deselectAllPresetToggles();
            toggles[i]->setValue( true );
            _loadPreset( value );
            return;
        }
    }
    ofLog() << "could not find preset with name: " << value << " to load. fail.";
}

/////////////////private
void GUIManager::_loadPreset(string _name)
{
    ofLog() << "GUIManager::Load Preset: " << _name;
    presetLoadLock = true;
    ofDirectory parentDir = getPresetDir();
    ofDirectory presetDir( parentDir.path() + "/" + _name );
    
    
    if (presetDir.exists() && presetDir.isDirectory())
    {
        presetDir.listDir();
        for (int i=0; i<presetDir.numFiles(); i++)
        {
            string filePath = presetDir.getName( i );
            int f = filePath.find_last_of(".");
            string fileName = filePath.substr(0, f);
            if(presetExcludes[fileName])
            {
                //ofLog() << "--------------------------don't load excluded file: " << fileName;
                continue;
            }
            int n = pages.size();
            for (int i=0; i<n; i++) {
                ofxUILabel* title = (ofxUILabel*)pages[i]->getWidget("title");
                string label = title->getLabel();
                if(label == fileName)
                {
                    string scenePresetPath = presetDir.path() + "/" + filePath;
                    pages[i]->loadSettings( scenePresetPath );
                    ofLog() << "loading preset fileName: " << fileName;
                }
            }
        }
    }
    else
    {
        ofLogError() << "not a preset directory. fail.";
    }
    
    presetLoadLock = false;
}

void GUIManager::deletePreset()
{
    string current = getCurrentSelectedPresetName();
    if(current == "")
    {
        ofLog() << "no preset selected to delete";
        return;
    }
    ofLog()<< "delete preset: " << current;

    
    string presetDirPath = getPresetDir().path() + "/" + current;
    ofDirectory presetDir(presetDirPath);
    if (presetDir.exists() && presetDir.isDirectory())
    {
        ofLog() << "removing preset directory: " << current;
        presetDir.remove(true);
    }
}

void GUIManager::addPresetExclude(string pageTitle)
{
    presetExcludes[pageTitle] = true;
}

void GUIManager::deselectAllPresetToggles()
{
    //reset all buttons to turned off state
    vector<ofxUIToggle*> toggles = radioPreset->getToggles();
    for (int i=0; i<toggles.size(); i++)
    {
        toggles[i]->setValue( false );
    }
}

void GUIManager::update() {}

void GUIManager::draw() {}

void GUIManager::exit() {
    ofLogNotice("deleting gui for GUI and Pagelist");
    delete gui;
    delete pageList;
}

void GUIManager::updateAllPages() {
    int n = pages.size();
    for (int i=0; i<n; i++) {
        pages[i]->setColorBack(bgColour);
        pages[i]->update();
    }
    if (pageList != NULL) {
        pageList->setColorBack(bgColour);
        pageList->update();
    }
}


void GUIManager::guiPageListEvent(ofxUIEventArgs &e){
    string name = e.widget->getName();
    vector<string> tokens = ofSplitString(name, ":");
    if (tokens.size()>0) {
        int i = ofToInt(tokens[0]);
        currentPageNum = i;
        showPage(i);
    }
}

void GUIManager::guiEvent(ofxUIEventArgs &e){
    //ofLogNotice("GUIConfigurable guiEvent : " + e.widget->getName());
    
    if (e.widget->getKind() == OFX_UI_WIDGET_BUTTON) {
        ofxUIButton* w = (ofxUIButton*)e.widget;
        ofLog() << "button clicked: " << w->getName();
        if(e.widget->getName() == UI_SAVE_LABEL){
            gui->saveSettings(path);
        }
        else if(e.widget->getName() == "<" && w->getValue()){
            w->mouseReleased(0, 0, true);
            showPreviousPage();
        }
        else if(e.widget->getName() == ">" && w->getValue()){
            w->mouseReleased(0, 0, true);
            showNextPage();
        }
    }
    
    else if(e.widget->getName() == "bg r"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        bgColour.r = slider->getScaledValue();
        updateAllPages();
    } else if(e.widget->getName() == "bg g"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        bgColour.g = slider->getScaledValue();
        updateAllPages();
    } else if(e.widget->getName() == "bg b"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        bgColour.b = slider->getScaledValue();
        updateAllPages();
    } else if(e.widget->getName() == "bg a"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        bgColour.a = slider->getScaledValue();
        updateAllPages();
    } else if(e.widget->getName() == "padding"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        padding = slider->getScaledValue();
        updateAllPages();
    }
    
    string widgetName = e.widget->getName();
    
    if (widgetName == "_PRESET")
    {
        ofxUITextInput *textInput = (ofxUITextInput *) e.widget;
        if (textInput->getTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS)
        {
            textInput->setTextString("");
            newPresetName = textInput->getTextString();
        }
        else if( textInput->getTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER )
        {
            newPresetName = textInput->getTextString();
        }
        else
        {
            newPresetName = textInput->getTextString();
        }
    }
    
    
    if( widgetName == getCurrentSelectedPresetName() )
    {
        ofxUIToggle* t = (ofxUIToggle*)e.widget;
        if(!presetLoadLock)loadPreset();
    }
    
}


ofxUICanvas* GUIManager::addPage(ofxUICanvas* ui){
    pages.push_back(ui);
	return ui;
}

void GUIManager::showNextPage(){
    if (!pages.size() > 0) return;
    if (++currentPageNum > pages.size()-1) currentPageNum = 0;
    showPage(currentPageNum);
}

void GUIManager::showPreviousPage(){
    if (!pages.size() > 0) return;
    if (--currentPageNum < 0) currentPageNum = pages.size()-1;
    showPage(currentPageNum);
}

void GUIManager::showPage(int num){
    if (!pages.size() > 0) return;
    if (!isVisible) return;
    //ofLogNotice("GUIManager::showPage " + ofToString(num));
    hideAllPages();
    pages[num]->setVisible(true);
}

void GUIManager::showAllPages(){
    for (int i=0; i<pages.size(); i++) {
        pages[i]->setVisible(true);
    }
}

void GUIManager::hideAllPages(){
    for (int i=0; i<pages.size(); i++) {
        pages[i]->setVisible(false);
    }
}

void GUIManager::show(){
    if (!pages.size() > 0) return;
    isVisible = true;
    pageList->setVisible(true);
    showPage(currentPageNum);
}

void GUIManager::hide(){
    if (!pages.size() > 0) return;
    isVisible = false;
    pageList->setVisible(false);
    hideAllPages();
}


void GUIManager::keyPressed (int key) {
    switch (key) {
            
        case 14:        // 'ctrl + n'
            addPreset();
            break;
            
        case 19:        // 'ctrl + s'
            savePreset();
            break;
            
        case 4:         // 'ctrl + d'
            deletePreset();
            break;
            
        case '[':
        case '-':
            showPreviousPage();
            break;
        case ']':
        case '+':
            showNextPage();
            break;
        case ' ':
            if (isVisible) hide();
            else show();
            break;
        case 'c':
            if (isCursorVisible) ofHideCursor();
            else ofShowCursor();
            isCursorVisible = !isCursorVisible;
            break;
        default:
            break;
    }
}

void GUIManager::mouseDragged(int x, int y, int button){}

void GUIManager::mousePressed(int x, int y, int button){}

void GUIManager::mouseReleased(int x, int y, int button){}

void GUIManager::windowResized(int w, int h){}