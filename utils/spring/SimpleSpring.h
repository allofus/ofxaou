//
//  SimpleSpring.h
//  impression
//
//  Created by Chris Mullany on 12/09/2012.
//  Copyright (c) 2012 AllofUs. All rights reserved.
//

#pragma once

#include "ofMain.h"


struct SpringParticle {
    ofVec3f pos,vel,forces;
    float mass,inverse_mass,drag;
    
    SpringParticle(){}
    
    SpringParticle(ofVec3f pos, float mass, float drag = .96) {
        setup(pos, mass, drag);
    }
    
    void setup(ofVec3f pos, float mass, float drag = .96) {
        this->pos = pos;
        this->mass = mass;
        this->drag = drag;
        if (mass==0.0f) inverse_mass = 0;
        else if (mass<0.001) mass=0.001;
        if (mass!=0.0f) inverse_mass = 1/mass;
    }
    
    void update() {
        
        if (mass==0.0f) inverse_mass = 0;
        else if (mass<0.001) mass=0.001;
        if (mass!=0.0f) inverse_mass = 1/mass;
        
        forces *= inverse_mass;
        vel += forces;
        forces = 0;
        vel.limit(15);
        pos += vel;
        vel *= drag;
    }
    
    void addForce(ofVec3f oForce) {
        forces += oForce;
    }
};

struct Spring {
    SpringParticle *a, *b;
    float k;
    float rest_length;
    
    Spring(){}
    
    Spring(SpringParticle *a, SpringParticle *b, float k = .001){
        setup(a, b, k);
    }
    
    void setup(SpringParticle *a, SpringParticle *b, float k = .001) {
        this->a = a;
        this->b = b;
        this->k = k;
        rest_length = (b->pos - a->pos).length();
    }
    
    void update() {
        ofVec3f dir = b->pos - a->pos;
        float dist = dir.length();
        if (dist == 0.0) dist = 0.0001; // prevent division by zero
        float f = (rest_length - dist) * k; // linear force spring
        dir.normalize();
        a->addForce(dir * -f);
        b->addForce(dir * f);
    }
};