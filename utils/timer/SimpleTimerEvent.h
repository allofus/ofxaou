//
//  SimpleTimerEvent.h
//
//  Created by Chris Mullany on 18/09/2012.
//  Copyright (c) 2012 AllofUs. All rights reserved.
//

#pragma once
#include "ofMain.h"

enum SimpleTimerEventType {
    TIMER_STARTED,
    TIMER_COMPLETED
};

class SimpleTimerEvent : public ofEventArgs {
    
public:
    
    string message;
    SimpleTimerEventType type;
    
    SimpleTimerEvent(SimpleTimerEventType type) {
        this->type = type;
    }
    
    string toString() {
        switch (type) {
            case TIMER_STARTED :
                return "timer started event";
                break;
            case TIMER_COMPLETED :
                return "timer completed event";
                break;
            default :
                return "unkown timer event";
                break;
                
        }
    }
    
    // for app wide events, notify using the static events property below e.g.
    //
    //      static SimpleTimerEvent timerEvent(SimpleTimerEvent);
    //      ofNotifyEvent(SimpleTimerEvent::events, timerEvent);
    //
    // and listen to it as a static:
    //
    //      ofAddListener(SimpleTimerEvent::events, this, &CLASS::onSimpleTimerEvent);
    // 
    // otherwise, create a new event and args scoped to your class
    //
    //      ofEvent<SimpleTimerEvent> timerCompleteEvent;
    //      SimpleTimerEvent eventArgs(TIMER_COMPLETED);
    //      ofNotifyEvent(timerCompleteEvent, eventArgs);
    //
    // and listen to that instance:
    //
    //      ofAddListener(timer.timerCompleteEvent, this, &FlashMessage::onSimpleTimerEvent);
    //
    static ofEvent <SimpleTimerEvent> events;
};

