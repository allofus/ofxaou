//
//  SimpleTimer.h
//
//  Created by chris@allofus.com
//  Copyright (c) 2012 AllofUs. All rights reserved.
//

#pragma once

#include "ofMain.h"
#include "SimpleTimerEvent.h"

class SimpleTimer {
public:
    
    float startTime;
    float endTime;
    bool isSimpleTimerReached;
    bool isLooping;
    bool isActive;
    string name;
    ofEvent<SimpleTimerEvent> timerCompleteEvent;
	
    
    SimpleTimer(){
        isSimpleTimerReached = false;
        isLooping = false;
        isActive = false;
        name = "timer";
        //enableAppEvents();
        ofAddListener(ofEvents().update, this, &SimpleTimer::onUpdate);
    }
    
    float getTimeRemaining() {
        if (isActive) {
            float timer = ofGetElapsedTimeMillis() - startTime;
            return (endTime - timer)/1000;
        } else {
            return 0.;
        }
    }
    
    float getTimeElapsed() {
        if (isActive) {
            float timer = ofGetElapsedTimeMillis() - startTime;
            return timer / 1000;
        } else {
            return 0.;
        }
    }
    
	void setup(){}
    
	void update() {
        if (!isActive) return;
        float timer = ofGetElapsedTimeMillis() - startTime;
        if(timer >= endTime && !isSimpleTimerReached) {
            isSimpleTimerReached = true;
            if (isLooping) start(endTime/1000, true);
            else stop();
            // dispatch event
            SimpleTimerEvent eventArgs(TIMER_COMPLETED);
            ofNotifyEvent(timerCompleteEvent, eventArgs);
        }
    }
    
    
    // duration in seconds
    void start(float duration, bool loop=false) {
        startTime = ofGetElapsedTimeMillis();
        endTime = duration*1000;
        isLooping = loop;
        isSimpleTimerReached = false;
        isActive = true;
    }
    
    void stop() {
        isSimpleTimerReached = false;
        isLooping = false;
        isActive = false;
    }
	
    void onUpdate(ofEventArgs &e) {
        update();
    }
    
    
};